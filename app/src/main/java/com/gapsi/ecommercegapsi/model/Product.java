package com.gapsi.ecommercegapsi.model;

import org.json.JSONException;
import org.json.JSONObject;

public final class Product {

    private String id = "";
    private double price = 0.0;
    private String pathImg = "";
    private String title = "";

    public Product(JSONObject json) throws JSONException {
        this.id = json.getString("id");
        this.price = json.getDouble("price");
        this.pathImg = json.getString("image");
        this.title = json.getString("title");
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
