package com.gapsi.ecommercegapsi.http;

import java.util.HashMap;

public class HttpCall {

    public static final int GET = 1;
    public static final int POST = 2;

    private String url;
    private int methodtype;
    private HashMap<String, String> params;
    private HashMap<String, String> headers;

    public static int getGET() {
        return GET;
    }

    public static int getPOST() {
        return POST;
    }

    public String getUrl() {
        return url;
    }

    public HttpCall setUrl(String url) {
        this.url = url;
        return this;
    }

    public int getMethodtype() {
        return methodtype;
    }

    public HttpCall setMethodtype(int methodtype) {
        this.methodtype = methodtype;
        return this;
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public HttpCall setParams(HashMap<String, String> params) {
        this.params = params;
        return this;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public HttpCall setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
        return this;
    }
}
