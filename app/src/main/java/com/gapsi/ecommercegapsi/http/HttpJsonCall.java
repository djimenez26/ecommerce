package com.gapsi.ecommercegapsi.http;

import org.json.JSONObject;


public class HttpJsonCall {

    public static final int POST = 2;

    private String url;
    private JSONObject params;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }
}