package com.gapsi.ecommercegapsi.http;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpRequest extends AsyncTask<HttpCall, String, String> {
    static final String COOKIES_HEADER = "Set-Cookie";
    public static String cookieId = "";
    static java.net.CookieManager msCookieManager = new java.net.CookieManager();

    public AsyncResponse delegate = null;

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public HttpRequest(AsyncResponse delegate) {
        this.delegate = delegate;
    }


    @Override
    protected String doInBackground(HttpCall... params) {
        HttpURLConnection urlConnection = null;
        HttpCall httpCall = params[0];

        StringBuilder response = new StringBuilder();
        try {
            String dataParams = getDataString(httpCall.getParams(), httpCall.getMethodtype());
            Log.i("dataParams", dataParams.toString());
            URL url = new URL(httpCall.getMethodtype() == HttpCall.GET ? httpCall.getUrl() + dataParams : httpCall.getUrl());
            Log.i("url", url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(httpCall.getMethodtype() == HttpCall.GET ? "GET" : "POST");
            Log.i("getMethodtype", urlConnection.getRequestMethod());
            setHeaders(httpCall.getHeaders(), urlConnection);
            Log.i("headers", urlConnection.getRequestProperties().toString());
            if(httpCall.getParams() != null && httpCall.getMethodtype() == HttpCall.POST) {
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                if (msCookieManager.getCookieStore().getCookies().size() > 0) {
                    // While joining the Cookies, use ',' or ';' as needed. Most of the servers are using ';'
                    urlConnection.setRequestProperty("Cookie",
                            TextUtils.join(";",  msCookieManager.getCookieStore().getCookies()));
                }
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.append(dataParams);
                writer.flush();
                writer.close();
                os.close();
            }
            int responseCode = urlConnection.getResponseCode();
            Log.i("responseCode","" + responseCode);

            if(httpCall.getMethodtype() == HttpCall.GET) {

                Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
                Log.i("allCookie","" + headerFields);
                List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);
                Log.i("cookieHeader","" + cookiesHeader);
                if (cookiesHeader != null) {
                    for (String cookie : cookiesHeader) {
                        msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                        Log.i("cookie","" + cookie);
                    }
                }
            }

            if(responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while((line = br.readLine()) != null) {
                    response.append(line);
                }
            } else {
                Log.i("responseText","" + urlConnection.getResponseMessage());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        delegate.processFinish(s);
    }


    private void setHeaders(HashMap<String, String> headers, HttpURLConnection urlConnection) {
        if (headers != null) {
            for(Map.Entry<String, String> header : headers.entrySet()) {
                urlConnection.setRequestProperty(header.getKey(), header.getValue());
            }
        }

    }

    private String getDataString(HashMap<String, String> params, int methodType) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean isFirst = true;
        if(params != null) {
            for(Map.Entry<String, String> entry : params.entrySet()) {

                if(isFirst) {
                    isFirst = false;
                    if(methodType == HttpCall.GET) {
                        result.append("?");
                    }
                } else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));

            }
        }

        return result.toString();
    }
}

