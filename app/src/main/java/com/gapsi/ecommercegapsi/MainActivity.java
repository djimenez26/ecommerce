package com.gapsi.ecommercegapsi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = (item) -> {
      switch (item.getItemId()) {
          case R.id.navigation_home:
              setTitle("Inicio");
              getSupportFragmentManager().beginTransaction().replace(R.id.menuFrame,new Home()).commit();

              return true;
          case R.id.navigation_search:
              setTitle("Búsqueda");
              getSupportFragmentManager().beginTransaction().replace(R.id.menuFrame,new Search()).commit();
              return true;
      }
      return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(navListener);

        //default fragment view
        if(savedInstanceState == null) {
            setTitle("Inicio");
            getSupportFragmentManager().beginTransaction().replace(R.id.menuFrame,new Home()).commit();
        }
    }


}