package com.gapsi.ecommercegapsi;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.gapsi.ecommercegapsi.adapters.ProductsListAdapter;
import com.gapsi.ecommercegapsi.http.HttpCall;
import com.gapsi.ecommercegapsi.http.HttpRequest;
import com.gapsi.ecommercegapsi.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Search extends Fragment {

    private static String url = "https://00672285.us-south.apigw.appdomain.cloud/demo-gapsi";
    private static String apiKey = "adb8204d-d574-4394-8c1a-53226a40876e";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_search, container, false);
        final ListView lv1 = (ListView) rootView.findViewById(R.id.custom_list);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (!prefs.getString("queryText", "").equals("")) {
            String query = prefs.getString("queryText", "");
            ArrayList<Product> productsResponse = new ArrayList();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("X-IBM-Client-Id", apiKey);
            HttpCall httpCall = new HttpCall();
            httpCall.setMethodtype(HttpCall.GET)
                    .setUrl(url + "/search?&query=" + query)
                    .setHeaders(headers);
            new HttpRequest(new HttpRequest.AsyncResponse() {
                @Override
                public void processFinish(String response) {
                    //Log.i("response", response.toString());
                    JSONObject jsonResponse = null;
                    try {
                        jsonResponse = new JSONObject(response.toString());
                        JSONArray jsonArrayReponse = jsonResponse.getJSONArray("items");
                        for(int i = 0; i < jsonArrayReponse.length(); i++) {
                            productsResponse.add(new Product(jsonArrayReponse.getJSONObject(i)));
                        }
                        Log.i("dataResponse", productsResponse.toString());
                        lv1.setAdapter(new ProductsListAdapter(rootView.getContext(), productsResponse));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).execute(httpCall);
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        final ViewGroup rootView = (ViewGroup) layoutInflater.inflate(R.layout.list_row_products_layout,(ViewGroup) getView().getParent());
        final ListView lv1 = (ListView) rootView.findViewById(R.id.custom_list);

        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((MainActivity) getContext()).getSupportActionBar().getThemedContext());
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setActionView(searchView);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("queryText", query);
                editor.commit();
                ArrayList<Product> productsResponse = new ArrayList();
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-IBM-Client-Id", apiKey);
                HttpCall httpCall = new HttpCall();
                httpCall.setMethodtype(HttpCall.GET)
                        .setUrl(url + "/search?&query=" + query)
                        .setHeaders(headers);
                new HttpRequest(new HttpRequest.AsyncResponse() {
                    @Override
                    public void processFinish(String response) {
                        //Log.i("response", response.toString());
                        JSONObject jsonResponse = null;
                        try {
                            jsonResponse = new JSONObject(response.toString());
                            JSONArray jsonArrayReponse = jsonResponse.getJSONArray("items");
                            for(int i = 0; i < jsonArrayReponse.length(); i++) {
                                productsResponse.add(new Product(jsonArrayReponse.getJSONObject(i)));
                            }
                            Log.i("dataResponse", productsResponse.toString());
                            lv1.setAdapter(new ProductsListAdapter(rootView.getContext(), productsResponse));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute(httpCall);
                //ArrayList<Product> array = ProductProvider.getByName(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        }));
    }
}