package com.gapsi.ecommercegapsi.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gapsi.ecommercegapsi.R;
import com.gapsi.ecommercegapsi.model.Product;

import java.util.ArrayList;

public class ProductsListAdapter extends BaseAdapter {

    private ArrayList<Product> listData;
    private LayoutInflater layoutInflater;

    public ProductsListAdapter(Context context, ArrayList<Product> listData) {
        this.listData = listData;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            convertView = this.layoutInflater.inflate(R.layout.list_row_products_layout, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.imgView = (ImageView) convertView.findViewById(R.id.imgProduct);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

    if(holder != null) {
        holder.title.setText(listData.get(position).getTitle());
        holder.price.setText("$" + listData.get(position).getPrice());
        //new ImageLoadTask(listData.get(position).getPathImg(), holder.imgView);
        Glide.with(convertView).load(listData.get(position).getPathImg()).into(holder.imgView);
    }



        return convertView;
    }

    static class ViewHolder {
        ImageView imgView;
        TextView title;
        TextView price;
    }
}
